#include "pcf8574.h"
/******************
笔记：
1、PCF8574寻址：1帧1字节，[0100 A2 A1 A0 R1/W0]
2、
3、
*******************/

/******************************
函数名：PCF8574ReadByte
功  能：读取端口状态
形  参：I--器件信息结构体  ch--用于存放端口状态的变量
返回值：0--正常 1--错误
备  注：
*******************************/
IIC_STATUS PCF8574ReadByte(const IIC_PIN *I, uint8_t *ch)
{
    uint8_t tmp;
    IIC_STATUS status;

    IIC_Start(I);
    IIC_WriteByte(I, I->ADDR | 0x01); //发送器件地址，并切换为读取模式
    status = IIC_Wait_ACK(I);
    if(status)
    {
        return(IIC_WRITE_ERR);
    }
    else
    {
        IIC_ReadByte(I, &tmp);   //读取数据
        IIC_NACK(I);
        IIC_Stop(I);
        *ch = tmp;
        return(IIC_OK);
    }
}

/******************************
函数名：PCF8574WriteByte
功  能：写入端口状态
形  参：I--器件信息结构体  ch--欲写入端口状态的变量
返回值：0--正常 1--错误
备  注：
*******************************/
IIC_STATUS PCF8574WriteByte(const IIC_PIN *I, uint8_t ch)
{
    IIC_STATUS status;

    IIC_Start(I);
    IIC_WriteByte(I, I->ADDR); //发送器件地址，并切换为写入模式
    status = IIC_Wait_ACK(I);
    if(status)
    {
        return(IIC_WRITE_ERR);
    }
    else
    {
        IIC_WriteByte(I, ch);      //数据写入
        status |= IIC_Wait_ACK(I);
        IIC_Stop(I);   //停止
        if(status)
        {
            return(IIC_WRITE_ERR);
        }
        else
        {
            return(IIC_OK);
        }
    }
}
