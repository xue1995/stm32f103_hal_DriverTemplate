#ifndef __UART_EXT_HEADER__
#define __UART_EXT_HEADER__
#include "stm32f1xx_hal.h"
#include "usart.h"
#include "cmsis_os.h"
#define UART_RX_BUF_LEN      128  
//#define UART_TX_BUF_LEN      128
#define UART_SENDING    1             //发送未完成  
#define UART_SENDOVER   0             //发送完成  

#define   DMA_MODE            1
#define   IT_MODE             0

#define sendmsg_Delay(huart,ExTxBuffer,...) sprintf((char*)ExTxBuffer, ##__VA_ARGS__);\
                                            UartPutStr(&huart, ExTxBuffer, strlen((char *)ExTxBuffer));\
                                            osDelay(strlen((char*)ExTxBuffer)+1)
#define sendmsg(huart,ExTxBuffer,...) sprintf((char*)ExTxBuffer, ##__VA_ARGS__);\
                                      UartPutStr(&huart, ExTxBuffer, strlen((char *)ExTxBuffer))

typedef struct  uart_node{  
  UART_HandleTypeDef      *huart;                       //串口
  uint8_t                 rx_finish_flag:1;             //空闲接收标记  
  uint8_t                 tx_finish_flag:1;             //发送完成标记  
  uint8_t                 tx_mode:1;                    //1: 表示用DMA模式，0: 表示用中断模式
  uint8_t                 duplex_mode:1;                //1: 全双工，0: 半双工设置
  uint8_t                 rs485_ctr_invert:1;           // RS485方向控制脚是否反转
  GPIO_TypeDef*           rs485Port;                    //半双工时，方向切换IO所用的端口
  uint16_t                rs485Pin;                     //半双工时，方向切换IO所用的PIN脚
  uint16_t                rxlen;                        //接收长度
  uint8_t                 *rxbuf;                       //接收缓存  
  //uint8_t                 *txbuf;
  struct uart_node        *next;
}USART_RECEIVETYPE;

//void UartExtInit(UART_HandleTypeDef *huart, uint32_t MODE, GPIO_TypeDef* port, uint16_t pin, uint8_t rs485_ctr_invert);
//void RS485ExtInit(UART_HandleTypeDef *huart, uint32_t mode, GPIO_TypeDef* port, uint16_t pin, uint8_t rs485_ctr_invert);
//void UartExtInit(UART_HandleTypeDef *huart, UART_TRANSCEIVER_MODE tr_mode, UART_WORK_MODE wmode, GPIO_TypeDef* port, uint16_t pin, uint8_t rs485_ctr_invert);
void UartExtInit(UART_HandleTypeDef *huart, uint32_t WorkMode);
int8_t UartExtRS485(UART_HandleTypeDef *huart, GPIO_TypeDef* port, uint16_t pin, uint8_t rs485_ctr_invert);
void UartPutStr(UART_HandleTypeDef *huart, const uint8_t *pdata, uint16_t Length);
uint16_t UartGetStr(UART_HandleTypeDef *huart, uint8_t *pdata);
void UartTxFinish(UART_HandleTypeDef *huart, uint32_t timeout);
#endif

