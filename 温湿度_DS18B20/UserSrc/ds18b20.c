/*************笔记****************
1、CubeMX 定义任意一个引脚，作为数据脚，并对引脚作出如下配置：
   GPlO output level       －－LOW
   GPIO mode               －－Output open drai
   GPIO Pull-up/Pull-down  －－No pull-up and no pull-down
   Maximum output speed    －－LOW
   User label              －－DS18S20
   ---------------------------------------------------------
***********************************/
#include <ds18b20.h>

#define FALSE 0
#define TRUE  1

// TEST BUILD
const uint8_t dscrc_table[256] =
{
    0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
    0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
    0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
    0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
    0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
    0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
    0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
    0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
    0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
    0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
    0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
    0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
    0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
    0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
    0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
    0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35
};
/**********************************************************************/
// 功能描述：设定使用到的IO口
// 输入参数：DS:结构体  port：IO端口， pin： IO pin脚
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
void    OWInitIO(T_OW_TYPE *DS, GPIO_TypeDef * port, uint16_t pin)
{
    //初始化DS18B20使用的IO管脚
    DS->PORT = port;
    DS->PIN = pin;
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
}
/**********************************************************************/
// 功能描述：进行MAXIM CRC8的校验
// 输入参数：crc: 前一次校验的值，等计算值
// 输出参数：校验计算结果
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWCRC(uint8_t crc, uint8_t value)
{
    return dscrc_table[crc ^ value];
}

/**********************************************************************/
// 功能描述：进行us级别的延时
// 输入参数：x: 待延时参数
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
//#pragma optimize= none
void ds18b20_delay_us(uint32_t x)
{
    uint32_t _dcnt;
    _dcnt = (x * 90) / 10; //_dcnt=(x*337)/10;
    while(_dcnt-- > 0)
        continue;
}
//void ds18b20_delay_us(uint32_t us)
//{
//  uint32_t tick,dly,tmp;
//
//  tick = osKernelSysTick();
//  dly = us * 168;
//  for(;;){
//    tmp = osKernelSysTick();
//    if((tmp - tick) > dly)
//      break;
//  }
//}
/**********************************************************************/
// 功能描述：复位总线上的设备
// 输入参数：pin: 总线使用的IO端口与PIN脚
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWReset(T_OW_TYPE *DS)
{
    GPIO_PinState resport;

    DisableInt();
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_RESET);
    ds18b20_delay_us(500);  //500us （该时间的时间范围可以从480到960微秒）
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
    ds18b20_delay_us(100);  // 70us DS18B20等待时间为15us~60us，所以等待时间不能小于60us
    //
    resport = HAL_GPIO_ReadPin(DS->PORT, DS->PIN);
    EnableInt();
    ds18b20_delay_us(100);  //100us  //等待时间不能小于70us
    if( resport == GPIO_PIN_SET)
        return 1;
    else
        return 0;
}
/**********************************************************************/
// 功能描述：向总线上设备写入一位数据
// 输入参数：pin: 总线使用的IO端口与PIN脚, bit:待写入值 0 or 1
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
void OWWriteBit(T_OW_TYPE *DS, uint8_t bit)
{
    DisableInt();
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_RESET);
    ds18b20_delay_us(2);    //2us 写1开始前0时隙 >1us
    if(bit)
        HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);     //写1
    ds18b20_delay_us(60);    //数据建立15us后,DS18B20开始采样,采样时间在(15us~45us)
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
    ds18b20_delay_us(2);    //连续两位间应大于1us
    EnableInt();
}
/**********************************************************************/
// 功能描述：向总线上设备写入一个字节的数据
// 输入参数：pin: 总线使用的IO端口与PIN脚, Data:待写入字节
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
void OWWriteByte(T_OW_TYPE *DS, uint8_t Data)
{
    uint8_t i;
    DisableInt();
    for(i = 8; i > 0; i--)
    {
        HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_RESET);
        ds18b20_delay_us(2);    //2us 写1开始前0时隙 >1us
        if(Data & 0x01)
            HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);   //写1
        ds18b20_delay_us(60);    //数据建立15us后,DS18B20开始采样,采样时间在(15us~45us)
        HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
        ds18b20_delay_us(2);    //连续两位间应大于1us
        Data >>= 1;
    }
    EnableInt();
}
/**********************************************************************/
// 功能描述：从总线上设备读取一位的数据
// 输入参数：pin: 总线使用的IO端口与PIN脚
// 输出参数：读取到的位值 0或1
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWReadBit(T_OW_TYPE *DS)
{
    uint8_t   result;
    DisableInt();
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_RESET);
    ds18b20_delay_us(5);   //5us 必须大于1us
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
    ds18b20_delay_us(4);   //4us //必须在15us内读取结果
    if(HAL_GPIO_ReadPin(DS->PORT, DS->PIN) == GPIO_PIN_SET)
        result = 1;
    else
        result = 0;
    ds18b20_delay_us(45);   //45us,//手册建议最小为45us
    EnableInt();
    return result;
}
/**********************************************************************/
// 功能描述：从总线上设备读取一个字节的数据
// 输入参数：pin: 总线使用的IO端口与PIN脚
// 输出参数：读取到的字节
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWReadByte(T_OW_TYPE *DS)
{
    uint8_t i, Data;

    DisableInt();
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
    ds18b20_delay_us(5);   //6us 必须大于1us

    for(i = 8; i > 0; i--)
    {
        Data >>= 1;
        HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_RESET);
        ds18b20_delay_us(5);   //6us 必须大于1us
        HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
        ds18b20_delay_us(4);   //4us //数据15us后建立,主机采样时间在(<=45us)
        if(HAL_GPIO_ReadPin(DS->PORT, DS->PIN) == GPIO_PIN_SET)
            Data |= 0x80;
        else
            Data &= 0x7F;
        ds18b20_delay_us(45);   //45us,//手册建议最小为45us
    }
    EnableInt();
    return Data;
}


/**********************************************************************/
// 功能描述：搜索总线上的一线设备的ROM ID
// 输入参数：pin: 总线使用的IO端口与PIN脚， ROM_ID：搜索到的ROM ID
// 输出参数：0 ：没有搜索到设备 1： 搜索到了设备
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWSearch(T_OW_TYPE *DS, uint8_t *ROM_ID)
{
    uint8_t   fstbit, sndbit, wrbit;      //第一次读取结果,第二次读取结果,写数据
    uint8_t   last_zero = 0;
    uint8_t   i;

    if(DS->LastDeviceFlag == FALSE)   //是不是检测到最后了
    {
        if(OWReset(DS) == 0)                  //复位总线等待ACK
        {
            OWWriteByte(DS, SearchROM);       //写入ROM搜索命令
            for(i = 1; i <= 64; i++)          //对64位ID的二叉树进行读二写一检测
            {
                fstbit = OWReadBit(DS);
                sndbit = OWReadBit(DS);         //读二
                if(fstbit == 1 && sndbit == 1)  //总线上没有器件
                {
                    return FALSE;
                }
                else        //总线上有器件
                {
                    if(i < DS->LastDiscrepancy)   //比较当前写入节点位置是否小于上次最后分歧节点
                    {
                        wrbit = (DS->LastRomId >> (i - 1)) & 0x01;  //是，写入上次节点的选择值
                        if((fstbit == sndbit) && (wrbit == 0))      //如果有分岐,并且上次分支走的是0(为1，则说明该节点0以下分支已经访问过，不在记录)
                        {
                            last_zero = i;        //记录分岐点（此处是找到离上次分岐最近的上一个分岐节点）
                        }
                    }
                    else
                    {
                        if(fstbit == !sndbit)   //总线上存在器件，该位值均为fstbit
                        {
                            wrbit = fstbit;
                        }
                        else      //总线上存在器件，该位值有1也有0
                        {
                            if(DS->LastDiscrepancy == i)      //如果分岐点与上次一致
                            {
                                wrbit = 0x01;                 //则改走1分支
                                DS->LastDiscrepancy = last_zero;    //将最近分岐节点上移
                            }
                            else
                            {
                                DS->LastDiscrepancy = i;               //否则为新的分岐节点，分岐节点记录位置下移
                                wrbit = 0x00;                          //第一次走0
                            }
                        }
                        //将待走节点分支记录到LASTROM ID中
                        if(wrbit == 0)
                            DS->LastRomId &= ~((uint64_t)0x01 << (i - 1));
                        else
                            DS->LastRomId |= (uint64_t)0x01 << (i - 1);
                    }
                    OWWriteBit(DS, wrbit); //写一
                }
            }
            if(DS->LastDiscrepancy == 0)      //如果最终分岐点为0，表明二叉树遍历完成
                DS->LastDeviceFlag = TRUE;      //遍历完成
            else
                DS->LastDeviceFlag = FALSE; //总结上还存在没有遍历的设备
            for(i = 0; i < 8; i++)
                ROM_ID[i] = DS->LastRomId >> ((i) * 8);
            uint8_t crc8 = 0;
            for(i = 0; i < 7; i++) //对读取到的ROM ID 进行CRC8检验，看是否出错
                crc8 = OWCRC(crc8, ROM_ID[i]);
            if (crc8 == ROM_ID[7])
                return TRUE;      //无错
            else
                return FALSE;       //出错
        }
        else        //复位不成功
        {
            return FALSE;
        }
    }
    else      //总线上设备已遍历完成
    {
        return FALSE;
    }
}
/**********************************************************************/
// 功能描述：第一次搜索总线上的设备的ROM ID
// 输入参数：pin: 总线使用的IO端口与PIN脚， ROM_ID：搜索到的ROM ID
// 输出参数：0 ：没有搜索到设备 1： 搜索到了设备
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWFirst(T_OW_TYPE *DS, uint8_t *ROM_ID)
{
    // reset the search state
    DS->LastDiscrepancy = 0;
    DS->LastDeviceFlag = FALSE;
    DS->LastRomId = 0;
    return OWSearch(DS, ROM_ID);
}

/**********************************************************************/
// 功能描述：搜索总线上下一个的设备的ROM ID  (调用前，必须执行一次OWFirst)
// 输入参数：pin: 总线使用的IO端口与PIN脚， ROM_ID：搜索到的ROM ID
// 输出参数：0 ：没有搜索到设备 1： 搜索到了设备
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
uint8_t OWNext(T_OW_TYPE *DS, uint8_t *ROM_ID)
{
    return OWSearch(DS, ROM_ID);
}
/**********************************************************************/
// 功能描述：读取指定ROM ID设备的温度值 (命令下发后，要等待750ms转换完成后再读取)
// 输入参数：pin: 总线使用的IO端口与PIN脚， ROM_ID：指定的ROM ID
// 输出参数：读取到的温度值
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
void OWStartConvert(T_OW_TYPE *DS, uint8_t *ROM_ID)
{
    uint8_t i;
    OWReset(DS);
    OWWriteByte(DS, MatchROM);        //ROM匹配命令
    for(i = 0; i < 8; i++)
        OWWriteByte(DS, ROM_ID[i]); //发送匹配的ROM
    OWWriteByte(DS, StartConvert); //发送转换命令
    OWReset(DS);
}
/**********************************************************************/
// 功能描述：读取指定ROM ID设备的温度值 (转换命令下发后，要等待750ms转换完成后再读取)
// 输入参数：pin: 总线使用的IO端口与PIN脚， ROM_ID：指定的ROM ID
// 输出参数：读取到的温度值
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
int16_t OWReadTemperture(T_OW_TYPE *DS, uint8_t *ROM_ID)
{
    uint8_t i;
    union
    {
        uint8_t bValue[2];
        int16_t dwValue;
    } Temp;

    OWReset(DS);
    OWWriteByte(DS, MatchROM);                //ROM匹配命令
    for(i = 0; i < 8; i++)
        OWWriteByte(DS, ROM_ID[i]);         //发送匹配的ROM
    OWWriteByte(DS, ReadScratchpad);      //发送读暂存器命令
    Temp.bValue[0] = OWReadByte(DS);  //读取低8位
    Temp.bValue[1] = OWReadByte(DS);  //读取高8位
    OWReset(DS);
    return Temp.dwValue;
}
/**********************************************************************/
// 功能描述：初始化DS18B20（跳过ROM ID匹配,用于总线上只有一个DS18B20的场景）
// 输入参数：pin: 总线使用的IO端口与PIN脚
// 输出参数：无
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
void OWSingleInit(T_OW_TYPE *DS, GPIO_TypeDef * port, uint16_t pin)
{
    DS->PORT = port;
    DS->PIN = pin;
    HAL_GPIO_WritePin(DS->PORT, DS->PIN, GPIO_PIN_SET);
    OWReset(DS);
    OWWriteByte(DS, SkipROM);
    OWWriteByte(DS, WriteScratchpad);
    OWWriteByte(DS, DS_AlarmTL);
    OWWriteByte(DS, DS_AlarmTH);
    OWWriteByte(DS, DS_PRECISION);
    OWReset(DS);
    OWWriteByte(DS, SkipROM);
    OWWriteByte(DS, StartConvert);
}
/**********************************************************************/
// 功能描述：读取DS18B20的值（跳过ROM ID匹配,用于总线上只有一个DS18B20的场景）
// 输入参数：pin: 总线使用的IO端口与PIN脚
// 输出参数：读取到的温度值
// 返 回 值：无
// 编写时间：2015.11.17
// 仿   者：胡安勤
// 修改记录:
/**********************************************************************/
int16_t OWSingleReadTemp(T_OW_TYPE *DS)
{
    union
    {
        uint8_t bValue[2];
        int16_t dwValue;
    } Temp;


    OWReset(DS);
    OWWriteByte(DS, SkipROM);
    OWWriteByte(DS, ReadScratchpad);
    Temp.bValue[0] = OWReadByte(DS);  //读取低8位
    Temp.bValue[1] = OWReadByte(DS);  //读取高8位
    OWReset(DS);
    OWWriteByte(DS, SkipROM);
    OWWriteByte(DS, StartConvert);
    return Temp.dwValue;
}

