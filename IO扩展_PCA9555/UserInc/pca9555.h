#ifndef __PCA9555_HEAD_
#define __PCA9555_HEAD_

#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "cmsis_os.h"
#include "hal_iic.h"

/********************* 个人自定义PAC9555的寄存器参数值***********************/
#define	PCA9555_POLARITY0				0x00									//极性反转值0(0不反转 1反转,PIN脚为输入时有效,)
#define	PCA9555_POLARITY1				0x00									//极性反转值1
#define	PCA9555_DERECTION0			0xFF									//方向配置值0 (0输出 1输入)
#define	PCA9555_DERECTION1			0x0F									//方向配置值1  高4位输出脚，低四位输入脚

/********************* 定义PAC9555的寄存器地址 ***********************/
#define	PCA9555_REG_IN0					0x00									//输入寄存器0地址    	
#define	PCA9555_REG_IN1					0x01									//输入寄存器1地址    
#define	PCA9555_REG_OUT0				0x02									//输出寄存器0地址    
#define	PCA9555_REG_OUT1				0x03									//输出寄存器1地址    
#define	PCA9555_REG_POL0				0x04									//极性反转寄存器0地址(PIN脚为输入时有效)    
#define	PCA9555_REG_POL1				0x05									//极性反转寄存器1地址 
#define	PCA9555_REG_CFG0				0x06									//方向配置寄存器0地址    
#define	PCA9555_REG_CFG1				0x07									//方向配置寄存器1地址


typedef struct{
  GPIO_TypeDef* port;
  uint16_t      pin;
}T_GPIO_TYPE;

/*存放PCA9555扩展口的引脚电平状态*/
typedef union{
  uint8_t  input[2];
  uint16_t data;
}U_INPUT;

/*需要对PCA9555扩展口的输出引脚设置电平信息*/
typedef union{
  uint8_t  output[2];
  uint16_t data;
}U_OUTPUT;

extern U_INPUT	EX_IN;
extern U_OUTPUT EX_OUT;
IIC_STATUS PCA9555Init(const IIC_PIN *PIN);
IIC_STATUS PCA9555GetPin(const IIC_PIN *PIN,U_INPUT *pin);
IIC_STATUS PCA9555SetPin(const IIC_PIN *PIN,const U_OUTPUT *pout);

#endif
