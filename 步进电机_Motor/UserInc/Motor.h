#ifndef _MOTOR_H
#define _MOTOR_H
#include "main.h"
/*
PB12---电机A相信号脚
PB13---电机B相信号脚
PB14---电机C相信号脚
PB15---电机D相信号脚
*/


void MotorCW(void);
void MotorCCW(void);
void MotorStop(void);
void Open_Door(void);
#endif
