#ifndef __EXT_GPIO_HEADER__
#define __EXT_GPIO_HEADER__
#include "main.h"

typedef struct
{
    GPIO_TypeDef* port;
    uint16_t      pin;
} T_GPIO_TYPE;

typedef union
{
    uint8_t  input[4];
    struct
    {
        uint8_t EX[3]; //外部引脚
        uint8_t X;     //内部引脚
    } pin;
    uint32_t data;
} U_INPUT;

typedef union
{
    uint8_t  output[4];
    struct
    {
        uint8_t EY[3]; //外部引脚
        uint8_t Y;     //内部引脚
    } pin;
    uint32_t data;
} U_OUTPUT;


extern U_INPUT   IN;
extern U_OUTPUT  OUT;
void init_ext_gpio(void);
void get_gpio_pins(U_INPUT *pin);
void set_gpio_pins(U_OUTPUT *pout);
uint8_t read_board_id(void);
uint8_t get_led_state(const U_OUTPUT *pout, uint8_t *pled);
#endif


