#include "extgpio.h"
#include "pcf8574.h"
#include "74hc595.h"
/******************
笔记：
1、X输入 Y输出
2、NPN(箭头向下)高电平时导通，PNP(箭头向上)低电平时导通；
3、PCF8574 IIC通讯  8个双向引脚 利用IIC引脚可写可读
4、74HC595 自带协议 8个单向引脚 引脚可写不可读
*******************/

#define  FREE_CONTROL 0     //释放气缸控制权 (1 释放 0 不释放)

const IIC_PIN pcf8574[3] =
{
    {
        .SCL_PORT  = IIC_SCL_GPIO_Port,
        .SCL_PIN   = IIC_SCL_Pin,
        .SDA_PORT  = IIC_SDA_GPIO_Port,
        .SDA_PIN   = IIC_SDA_Pin,
        .DelayTick = 10,
        .ADDR      = 0x42 //详细见PCF8574寻址
    },
    {
        .SCL_PORT  = IIC_SCL_GPIO_Port,
        .SCL_PIN   = IIC_SCL_Pin,
        .SDA_PORT  = IIC_SDA_GPIO_Port,
        .SDA_PIN   = IIC_SDA_Pin,
        .DelayTick = 10,
        .ADDR      = 0x44
    },
    {
        .SCL_PORT  = IIC_SCL_GPIO_Port,
        .SCL_PIN   = IIC_SCL_Pin,
        .SDA_PORT  = IIC_SDA_GPIO_Port,
        .SDA_PIN   = IIC_SDA_Pin,
        .DelayTick = 10,
        .ADDR      = 0x46
    }
};

const T_GPIO_TYPE INPUT_PINS[8] =        //输入端子板(内部)
{
    {.port = GPIOB, .pin = GPIO_PIN_12},
    {.port = GPIOB, .pin = GPIO_PIN_13},
    {.port = GPIOB, .pin = GPIO_PIN_14},
    {.port = GPIOB, .pin = GPIO_PIN_15},
    {.port = GPIOC, .pin = GPIO_PIN_6},
    {.port = GPIOC, .pin = GPIO_PIN_7},
    {.port = GPIOC, .pin = GPIO_PIN_8},
    {.port = GPIOA, .pin = GPIO_PIN_11}
};

const T_GPIO_TYPE OUTPUT_PINS[6] =       //输出端子板
{
    {.port = GPIOC, .pin = GPIO_PIN_5},
    {.port = GPIOC, .pin = GPIO_PIN_4},
    {.port = GPIOA, .pin = GPIO_PIN_7},
    {.port = GPIOA, .pin = GPIO_PIN_6},
    {.port = GPIOA, .pin = GPIO_PIN_5},
    {.port = GPIOA, .pin = GPIO_PIN_4}
};

const T_GPIO_TYPE id_pin[4] =            //单板地址拨码选择
{
    {.port = GPIOA, .pin = GPIO_PIN_12},
    {.port = GPIOB, .pin = GPIO_PIN_4},
    {.port = GPIOB, .pin = GPIO_PIN_8},
    {.port = GPIOB, .pin = GPIO_PIN_9}
};


U_INPUT   IN;
U_OUTPUT  OUT;

/******************************
函数名：init_ext_gpio
功  能：初始化外部gpio器件
形  参：无
返回值：
备  注：74HC595器件 3位输入 8位输出
*******************************/
void init_ext_gpio(void)
{
    hc595Init(&hc595);
}

/******************************
函数名：get_gpio_pins
功  能：读取内外部输入引脚的状态
形  参：pin--内外引脚的信息结构体
返回值：无，信息将直接赋予 结构体pin
备  注：
*******************************/
void get_gpio_pins(U_INPUT *pin)
{
    uint8_t i, XIN = 0;

    /*读外部扩展板PCF8574引脚*/
    for(i = 0; i < 3; i++)
    {
        PCF8574ReadByte(&pcf8574[i], &pin->pin.EX[i]);
    }
    /*读内部MUC引脚*/
    for(i = 0; i < 8; i++)
    {
        XIN >>= 1;
        if(HAL_GPIO_ReadPin(INPUT_PINS[i].port, INPUT_PINS[i].pin) == GPIO_PIN_RESET)
        {
            XIN &= ~0x80;
        }
        else
        {
            XIN |= 0x80;
        }
    }
    pin->pin.X = XIN;
}

/******************************
函数名：set_gpio_pins
功  能：设置74HC595的端口及MUC输出引脚状态
形  参：pout--内外引脚的状态结构体
返回值：无
备  注：
*******************************/
void set_gpio_pins(U_OUTPUT *pout)
{
    uint8_t i, XOUT;
    /*向74HC595发送引脚状态*/
    hc595WriteStr(&hc595, &pout->pin.EY[0], 3);

    XOUT = pout->pin.Y;
    for(i = 0; i < 6; i++)
    {
        HAL_GPIO_WritePin(OUTPUT_PINS[i].port, OUTPUT_PINS[i].pin, XOUT & 0x01 ? GPIO_PIN_SET : GPIO_PIN_RESET);
        XOUT >>= 1;
    }
}


/******************************
函数名：read_board_id
功  能：读取板子拨码开关状态
形  参：无
返回值：id--开关状态
备  注：此处默认拨码开关低电平有效,所以变量采用1作为有效位。
*******************************/
uint8_t read_board_id(void)
{
    uint8_t id, i;

    id = 0;
    for(i = 0; i < 4; i++)
    {
        id >>= 1;
        if(HAL_GPIO_ReadPin(id_pin[i].port, id_pin[i].pin) == GPIO_PIN_RESET)
        {
            id |= 0x08; //0000 1000
        }
    }
    return id;
}

/******************************
函数名：get_led_state
功  能：获取外部扩展板的输出引脚
形  参：pout--引脚信息结构体  pled--外部引脚状态
返回值：0
备  注：引脚状态字节，低6位有效
*******************************/
uint8_t get_led_state(const U_OUTPUT *pout, uint8_t *pled)
{
    uint8_t i;

    for(i = 0; i < 6; i++)
    {
        *pled++ = pout->pin.EY[i];
    }
    return 0;
}

