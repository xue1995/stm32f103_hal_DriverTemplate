#ifndef _MYCAN_H
#define _MYCAN_H
#include "main.h"
void CAN_Config(CAN_HandleTypeDef *phcan, uint8_t FIFO_Num);

uint8_t Can_TxMessage(CAN_HandleTypeDef *phcan, uint8_t ide, uint32_t id, uint8_t len, uint8_t *pdata);

void Can_Dome(CAN_HandleTypeDef *hcan1);
#endif
