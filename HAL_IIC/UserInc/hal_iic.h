#ifndef __IIC_Configuration_HEAD_
#define __IIC_Configuration_HEAD_

#include "main.h"
#define		GPIO_WRITE_PIN(PORT,PIN,VALUE)	HAL_GPIO_WritePin(PORT,PIN,(VALUE)?GPIO_PIN_SET:GPIO_PIN_RESET)
#define		GPIO_READ_PIN(PORT,PIN)					HAL_GPIO_ReadPin(PORT,PIN)

typedef enum  {
  IIC_OK           = 0x00,    //IIC工作正常
  IIC_WAIT_ACK_ERR = 0x01,    //slave设备返回ACK错误
  IIC_WRITE_ERR    = 0x02,    //向slave设备写入错误
  IIC_READ_ERR     = 0x04     //从slave设备读取错误
} IIC_STATUS;

typedef struct
{
  GPIO_TypeDef	*SCL_PORT;		//定义IIC时钟使用的端口
  uint16_t			SCL_PIN;			//定义IIC时钟使用的PIN脚
  GPIO_TypeDef	*SDA_PORT;   	//定义IIC数据使用的端口
  uint16_t			SDA_PIN;			//定义IIC数据使用的PIN脚
  uint32_t			DelayTick;   	//定义延时，以适应不同器件对速率的不同要求,具体值要在调试中确定
  uint8_t				ADDR;        	//器件地址
}IIC_PIN;

void IIC_Init(const IIC_PIN *I);
void IIC_Start (const IIC_PIN *I);
void IIC_Stop (const IIC_PIN *I);
IIC_STATUS IIC_Wait_ACK(const IIC_PIN *I);
void IIC_WriteByte(const IIC_PIN *I,uint8_t data);
void IIC_ReadByte(const IIC_PIN *I,uint8_t *data);
void IIC_ACK(const IIC_PIN *I);
void IIC_NACK(const IIC_PIN *I);

#endif
