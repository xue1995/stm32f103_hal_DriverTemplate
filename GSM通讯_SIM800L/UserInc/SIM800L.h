#ifndef _SIM800L_H
#define _SIM800L_H
#include "main.h"

uint8_t* SIM800L_Check_Cmd(uint8_t *str);
uint8_t SIM800L_Send_Cmd(uint8_t *cmd, uint8_t *ack, u16 WaitTime);

uint8_t SIM800L_Info_Show(void);
void SIM800L_CallNum(uint8_t *Num);
void SIM800L_CmdShowOff(void);
void SIM800L_SendEN_SMS(uint8_t *phone,uint8_t *text);
#endif
