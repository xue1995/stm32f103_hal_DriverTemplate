#ifndef _LCD16032_H
#define _LCD16032_H
#include "main.h"
void lcd16032_delay(uint8_t Nom);
void lcd16032_send_byte(uint8_t byte);
uint8_t lcd16032_read_byte(void);
uint8_t lcd16032_read_busy(void);
uint8_t lcd16032_write_cmd(uint8_t cmd);
uint8_t lcd16032_write_data(uint8_t byte);
void lcd16032_init(void);

uint8_t lcd16032_send_char(uint8_t byte);
uint8_t lcd16032_show_char(uint8_t x, uint8_t y, uint8_t byte);
uint8_t lcd16032_send_string(uint8_t *str);
uint8_t lcd16032_set_xy(uint8_t x, uint8_t y);

uint8_t lcd16032_show_string(uint8_t x, uint8_t y, uint8_t *str);

char* split(char * p, char * str);
#endif
