#include "74hc595.h"

/******************
功  能：定义HC595的功能引脚
备  注：变更引脚，由MX配置完成
*******************/
const T_HC595_PIN hc595 =
{
    .sck_port   = HC595_SCK_GPIO_Port,
    .sck_pin    = HC595_SCK_Pin,      //数据输入时钟线

    .data_port  = HC595_SI_GPIO_Port,
    .data_pin   = HC595_SI_Pin,       //数据线

    .rck_port   = HC595_RCK_GPIO_Port,
    .rck_pin    = HC595_RCK_Pin,      //输出存储器锁存时钟线

    .en_port    = HC595_OE_GPIO_Port,
    .en_pin     = HC595_OE_Pin        //芯片使能
};

/******************
函数名：hc595Delay
功  能：实现us级延迟
形  参：cnt--us值
返回值：无
备  注：
*******************/
void hc595Delay(volatile uint16_t cnt)
{
    while(cnt--)
        continue;
}

/******************************
函数名：hc595Init
功  能：初始化hc595的使能脚(EN)，默认工作状态
形  参：pin--hc595引脚结构体
返回值：
备  注：0--器件屏蔽  1--器件工作
*******************************/
void hc595Init(const T_HC595_PIN *pin)
{
    HAL_GPIO_WritePin(pin->en_port, pin->en_pin, GPIO_PIN_RESET);
}

/******************************
函数名：hc595DeInit
功  能：初始化hc595的使能脚(EN)，默认休息状态
形  参：pin--hc595引脚结构体
返回值：
备  注：0--器件屏蔽  1--器件工作
*******************************/
void hc595DeInit(const T_HC595_PIN *pin)
{
    HAL_GPIO_WritePin(pin->en_port, pin->en_pin, GPIO_PIN_SET);
}

/******************************
函数名：hc595WriteStr
功  能：向hc595输出字符
形  参：pin--引脚结构体 data--数据字节组 length--数据字节长度
返回值：
备  注：1、数据输出方向--高位先进，QH=高八位，QG，高七位，QF，高六位，依次下去。
        2、CLK：低电平->高电平 移位寄存器存储一个位
           RCK：低电平->高电平 输出移位寄存器中缓存的位。
笔  记：这里用的是三级串联，根据HC595的特性，移位寄存器只能存储8个位，
        如果移位寄存器的8个位填满后，再往移位寄存器中塞一个，移位寄存器的最后一个位数据会被挤出去，
        这个数据会从引脚9(SQH)发送给下个HC595引脚14(SI)。
*******************************/
void hc595WriteStr(const T_HC595_PIN *pin, uint8_t *data, uint16_t length)
{
    uint16_t i;
    uint8_t byte;

    data += length - 1; //等价于data[length - 1]
    while(length--)
    {
        byte = *data--;//取数组data[length - 1]赋予byte，然后再--data[length - 1]。
        for(i = 0; i < 8; i++)
        {
            HAL_GPIO_WritePin(pin->sck_port, pin->sck_pin, GPIO_PIN_RESET); //clk = 0 保持缓存区状态
            if(byte & 0x80) //从高位开始发送
            {
                HAL_GPIO_WritePin(pin->data_port, pin->data_pin, GPIO_PIN_SET);//data=1
            }
            else
            {
                HAL_GPIO_WritePin(pin->data_port, pin->data_pin, GPIO_PIN_RESET);//data=0
            }
            byte <<= 1;
            hc595Delay(10);//us
            HAL_GPIO_WritePin(pin->sck_port, pin->sck_pin, GPIO_PIN_SET); //clk = 1 存储数据至缓存区
            hc595Delay(10);//us
        }
    }
    HAL_GPIO_WritePin(pin->rck_port, pin->rck_pin, GPIO_PIN_RESET);
    hc595Delay(10);
    HAL_GPIO_WritePin(pin->rck_port, pin->rck_pin, GPIO_PIN_SET);//发送缓存区内容
}
