/*************笔记****************
1、ENC28J60_中文手册.pdf
2、控制寄存器的地址定义是一个组合地址：
   0-4bit 寄存器地址
   5-6bit 块区编号
   7bit   MAC/PHY indicator
3、
4、
5、
***********************************/
#include "main.h"
#include "enc28j60.h"
#include "cmsis_os.h"

static uint8_t Enc28j60Bank;
static uint32_t NextPacketPtr;

#define  ENC28J60_CS(N) HAL_GPIO_WritePin(ENC28J60_CS_GPIO_Port,ENC28J60_CS_Pin,(N)?GPIO_PIN_SET:GPIO_PIN_RESET)
/*********Start*************************底层命令*******************************Start*****************/
/*********************************************
函数名：SPI_ReadWrite
功  能：SPI读写1byte数据
形  参：pTxData--要发送的字节
返回值：收到的字节
备  注：CubeMX启动SPI配置，分频根据系统推荐。
！！！如果移植，需要在此填写SPI读写函数！！！
**********************************************/
extern SPI_HandleTypeDef hspi1;
static uint8_t SPI_ReadWrite(uint8_t pTxData)
{
    uint8_t pRxData;
    HAL_SPI_TransmitReceive(&hspi1, &pTxData, &pRxData, sizeof(pTxData), 10);
    return pRxData;
}
/*********************************************
函数名：ENC28J60_ReadOp
功  能：从器件寄存器读取状态
形  参：op--操作码  address--控制寄存器地址
返回值：寄存器状态值
备  注：命令时序图--29页
**********************************************/
uint8_t ENC28J60_ReadOp(uint8_t op, uint8_t address)
{
    uint8_t dat = 0;

    ENC28J60_CS(0);

    dat = op | (address & ADDR_MASK); //重构操作码和地址，参见手册第28页。
    SPI_ReadWrite(dat);

    dat = SPI_ReadWrite(0xFF);
    //如果需要，进行虚拟读取（对于MAC和MII，请参见手册第29页）
    if(address & 0x80)
    {
        dat = SPI_ReadWrite(0xFF);
    }
    // release CS
    ENC28J60_CS(1);
    return dat;
}

/*********************************************
函数名：ENC28J60_WriteOp
功  能：向器件寄存器写入1byte
形  参：op--操作码  address--控制寄存器地址  data--数据字节
返回值：
备  注：
**********************************************/
void ENC28J60_WriteOp(uint8_t op, uint8_t address, uint8_t data)
{
    uint8_t dat = 0;

    ENC28J60_CS(0);
    dat = op | (address & ADDR_MASK);//重构操作码和地址，参见手册第28页。
    SPI_ReadWrite(dat);

    SPI_ReadWrite(data);//写入数据
    ENC28J60_CS(1);
}

/*********************************************
函数名：ENC28J60_ReadBuffer
功  能：读取缓冲存储器的数据
形  参：len--长度   data--存放数据的指针
返回值：
备  注：
**********************************************/
void ENC28J60_ReadBuffer(uint32_t len, uint8_t* data)
{
    ENC28J60_CS(0);
    SPI_ReadWrite(ENC28J60_READ_BUF_MEM);
    while(len)
    {
        len--;
        *data = (uint8_t)SPI_ReadWrite(0);
        data++;
    }
    *data = '\0';
    ENC28J60_CS(1);
}
/*********************************************
函数名：ENC28J60_WriteBuffer
功  能：向缓冲存储器写入数据
形  参：len--字节长度   data--数据内容的指针
返回值：
备  注：
**********************************************/
void ENC28J60_WriteBuffer(uint32_t len, uint8_t* data)
{
    ENC28J60_CS(0);
    SPI_ReadWrite(ENC28J60_WRITE_BUF_MEM);
    while(len)
    {
        len--;
        SPI_ReadWrite(*data);
        data++;
    }
    ENC28J60_CS(1);
}
/*********************************************
函数名：ENC28J60_SetBank
功  能：设置块区
形  参：
返回值：
备  注：块区编号在寄存器地址的bit6 bit5
**********************************************/
void ENC28J60_SetBank(uint8_t address)
{
    /*计算本次寄存器地址在存取区域的位置*/
    if((address & BANK_MASK) != Enc28j60Bank)
    {
        /*清除ECON1的BSEL1 BSEL0 详见数据手册15页*/
        ENC28J60_WriteOp(ENC28J60_BIT_FIELD_CLR, ECON1, (ECON1_BSEL1 | ECON1_BSEL0));
        /*请注意寄存器地址的宏定义，bit6 bit5为寄存器存储区域位置*/
        ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, ECON1, (address & BANK_MASK) >> 5);
        /*重新确定当前寄存器存储区域*/
        Enc28j60Bank = (address & BANK_MASK);
    }
}
/*********END*************************底层命令*******************************END*****************/




/*********Start*************************中层命令*******************************Start*****************/
/*********************************************
函数名：ENC28J60_Read
功  能：向指定块区的寄存器地址读取状态值
形  参：address--寄存器地址
返回值：
备  注：
**********************************************/
uint8_t ENC28J60_Read(uint8_t address)
{
    // 设定寄存器地址区域
    ENC28J60_SetBank(address);
    // 读取寄存器值--发送读寄存器命令和地址
    return ENC28J60_ReadOp(ENC28J60_READ_CTRL_REG, address);
}

/*********************************************
函数名：ENC28J60_Write
功  能：向指定块区的寄存器地址写入数据
形  参：address--寄存器地址
返回值：
备  注：
**********************************************/
void ENC28J60_Write(uint8_t address, uint8_t data)
{
    // 设定寄存器地址区域
    ENC28J60_SetBank(address);
    // 写寄存器值--发送写寄存器命令和地址
    ENC28J60_WriteOp(ENC28J60_WRITE_CTRL_REG, address, data);
}
/*********END*************************中层命令*******************************END*****************/


/*********************************************
函数名：ENC28J60_PhyWrite
功  能：向PHY寄存器地址写入2byte
形  参：address--寄存器地址  data--数据字节
返回值：
备  注：详见手册21页
**********************************************/
void ENC28J60_PhyWrite(uint8_t address, uint16_t data)
{
    uint16_t retry = 0;
    //向MIREGADR写入地址
    ENC28J60_Write(MIREGADR, address);
    //写入低8位数据
    ENC28J60_Write(MIWRL, data);
    //写入高8位数据
    ENC28J60_Write(MIWRH, data >> 8);
    //等待PHY寄存器写入完成
    while(ENC28J60_Read(MISTAT) & MISTAT_BUSY && retry < 0XFFF)
    {
        retry++;
    }
}

/*********************************************
函数名：ENC28J60_PhyWrite
功  能：从PHY寄存器地址读取1byte
形  参：address--寄存器地址  data--数据字节
返回值：
备  注：详见手册21页
**********************************************/
uint16_t ENC28J60_PhyRead(uint8_t address)
{
    uint16_t retry = 0;
    uint16_t data;
    //向MIREGADR写入地址
    ENC28J60_Write(MIREGADR, address);
    ENC28J60_Write(MICMD, MICMD_MIIRD);//MII 读使能位
    //等待PHY寄存器操作完毕
    while((ENC28J60_Read(MISTAT) & MISTAT_BUSY) && retry < 0XFFF)
    {
        retry++;
    }
    ENC28J60_Write(MICMD, 0x00);//复位
    data = ENC28J60_Read(MIRDH) << 8;
    data |= ENC28J60_Read(MIRDL);
    return data;
}

/*********************************************
函数名：ENC28J60_clkout
功  能：将enc28j60第三引脚的时钟输出改为：
形  参：
返回值：
备  注：(本例程该引脚NC,没用到) enc28j60clkout(2);//from 6.25MHz to 12.5MHz
**********************************************/
void ENC28J60_clkout(uint8_t clk)
{
    //setup clkout: 2 is 12.5MHz:
    ENC28J60_Write(ECOCON, clk & 0x7);
}


/*********************************************
函数名：ENC28J60_getrev
功  能：读取器件版本号
形  参：
返回值：版本号
备  注：在EREVID内也存储了版本信息。
        EREVID 是一个只读控制寄存器。
         其中包含一个5位标识符，用来标识器件特定硅片的版本号
**********************************************/
uint8_t ENC28J60_getrev(void)
{
    return ENC28J60_Read(EREVID);
}



/*********************************************
函数名：ENC28J60_Init
功  能：初始化
形  参：
返回值：
备  注：
**********************************************/
int ENC28J60_Init(uint8_t* macaddr)
{
    uint16_t retry = 0;
    ENC28J60_WriteOp(ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET); //软件复位
    while(!(ENC28J60_Read(ESTAT)&ESTAT_CLKRDY) && retry < 500) //等待时钟稳定
    {
        retry++;
        osDelay(1);
    };
    if(retry >= 500)
        return 1; //ENC28J60初始化失败

    // 设置接收缓冲区起始地址
    NextPacketPtr = RXSTART_INIT; //数据包起始位赋值
    // 设置接收缓冲区 起始指针
    ENC28J60_Write(ERXSTL, RXSTART_INIT & 0xFF);
    ENC28J60_Write(ERXSTH, RXSTART_INIT >> 8);
    // 设置接收缓冲区 读指针
    ENC28J60_Write(ERXRDPTL, RXSTART_INIT & 0xFF);
    ENC28J60_Write(ERXRDPTH, RXSTART_INIT >> 8);
    // 设置接收缓冲区 结束指针
    ENC28J60_Write(ERXNDL, RXSTOP_INIT & 0xFF);
    ENC28J60_Write(ERXNDH, RXSTOP_INIT >> 8);
    /* 设置发送缓冲区 起始指针 */
    ENC28J60_Write(ETXSTL, TXSTART_INIT & 0xFF);
    ENC28J60_Write(ETXSTH, TXSTART_INIT >> 8);
    /* 设置发送缓冲区 结束指针 */
    ENC28J60_Write(ETXNDL, TXSTOP_INIT & 0xFF);
    ENC28J60_Write(ETXNDH, TXSTOP_INIT >> 8);
    // do bank 1 stuff, packet filter:
    // For broadcast packets we allow only ARP packtets
    // All other packets should be unicast only for our mac (MAADR)
    //
    // The pattern to match on is therefore
    // Type     ETH.DST
    // ARP      BROADCAST
    // 06 08 -- ff ff ff ff ff ff -> ip checksum for theses bytes=f7f9
    // in binary these poitions are:11 0000 0011 1111
    // This is hex 303F->EPMM0=0x3f,EPMM1=0x30
    /* 使能单播过滤 使能CRC校验 使能 格式匹配自动过滤*/
    ENC28J60_Write(ERXFCON, ERXFCON_UCEN | ERXFCON_CRCEN | ERXFCON_PMEN);
    ENC28J60_Write(EPMM0, 0x3f);
    ENC28J60_Write(EPMM1, 0x30);
    ENC28J60_Write(EPMCSL, 0xf9);
    ENC28J60_Write(EPMCSH, 0xf7);
    /* 使能MAC接收 允许MAC发送暂停控制帧 当接收到暂停控制帧时停止发送*/
    ENC28J60_Write(MACON1, MACON1_MARXEN | MACON1_TXPAUS | MACON1_RXPAUS);
    // 退出复位状态
    ENC28J60_Write(MACON2, 0x00);

    /* 用0填充所有短帧至60字节长 并追加一个CRC 发送CRC使能 帧长度校验使能 MAC全双工使能*/
    /* 提示 由于ENC28J60不支持802.3的自动协商机制， 所以对端的网络卡需要强制设置为全双工 */
    ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, MACON3, \
                     MACON3_PADCFG0 | \
                     MACON3_TXCRCEN | \
                     MACON3_FRMLNEN | \
                     MACON3_FULDPX);

    // 设置帧间间隙 (non-back-to-back)
    ENC28J60_Write(MAIPGL, 0x12);
    ENC28J60_Write(MAIPGH, 0x0C);

    // 设置帧间间隙 (back-to-back)
    ENC28J60_Write(MABBIPG, 0x15);

    // 设置控制器将接受的最大数据包大小
    // 不要发送超过max_framelen的数据包:
    ENC28J60_Write(MAMXFLL, MAX_FRAMELEN & 0xFF);
    ENC28J60_Write(MAMXFLH, MAX_FRAMELEN >> 8);

    // 写入MAC地址
    // NOTE: ENC28J60中的MAC地址是向后字节
    ENC28J60_Write(MAADR5, macaddr[0]);
    ENC28J60_Write(MAADR4, macaddr[1]);
    ENC28J60_Write(MAADR3, macaddr[2]);
    ENC28J60_Write(MAADR2, macaddr[3]);
    ENC28J60_Write(MAADR1, macaddr[4]);
    ENC28J60_Write(MAADR0, macaddr[5]);

    //配置PHY为全双工  LEDB为拉电流
    ENC28J60_PhyWrite(PHCON1, PHCON1_PDPXMD);

    // 半双工回环禁止
    ENC28J60_PhyWrite(PHCON2, PHCON2_HDLDIS);

    // 切换到 bank 0
    ENC28J60_SetBank(ECON1);

    // 使能中断 全局中断 接收中断 接收错误中断
    ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, EIE, EIE_INTIE | EIE_PKTIE);

    // 使能数据包接收位
    ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);


    if(ENC28J60_Read(MAADR5) == macaddr[0])
        return 0; //初始化成功
    else
        return 1;

}



/*********Start********************************以太网层*******************************Start*****************/
/*********************************************
函数名：ENC28J60_PacketSend
功  能：发送一次数据包
形  参：len--字节长度   packet--数据包指针
返回值：
备  注：参见手册42页
**********************************************/
void ENC28J60_PacketSend(uint32_t len, uint8_t* packet)
{
    // 设置发送缓冲区起始地址
    ENC28J60_Write(EWRPTL, TXSTART_INIT & 0xFF);
    ENC28J60_Write(EWRPTH, TXSTART_INIT >> 8);

    // 设置发送缓冲区结束地址 该值对应发送数据包长度
    ENC28J60_Write(ETXNDL, (TXSTART_INIT + len) & 0xFF);
    ENC28J60_Write(ETXNDH, (TXSTART_INIT + len) >> 8);

    // 发送之前发送控制包格式字 (0x00 表示使用Macon3设置)
    ENC28J60_WriteOp(ENC28J60_WRITE_BUF_MEM, 0, 0x00);

    // 将数据包发送到ENC28J60传输缓冲区
    ENC28J60_WriteBuffer(len, packet);

    // 将传输缓冲区的内容发送到网络层
    ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRTS);

    // 重置传输逻辑问题. See Rev. B4 Silicon Errata point 12.
    if( (ENC28J60_Read(EIR) & EIR_TXERIF) )
    {
        ENC28J60_WriteOp(ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRTS);
    }
}

// Gets a packet from the network receive buffer, if one is available.
// The packet will by headed by an ethernet header.
/*********************************************
函数名：ENC28J60_PacketReceive
功  能：读取一次数据包
形  参：maxlen--检索到的数据包的最大可接受长度  packet--存储数据包数据的指针。
返回值：如果检索到数据包，则以字节为单位的数据包长度，否则为零。
备  注：接收数据包结构示例 器件手册45页
**********************************************/
uint32_t ENC28J60_PacketReceive(uint32_t maxlen, uint8_t* packet)
{
    uint32_t rxstat;
    uint32_t len;

    // 检查是否已接收和缓冲数据包
    //if( !(ENC28J60_Read(EIR) & EIR_PKTIF) ){
    // 以上不起作用. See Rev. B4 Silicon Errata point 6.
    if( ENC28J60_Read(EPKTCNT) == 0 ) //收到的以太网数据包长度
    {
        return(0);
    }

    // 将读取指针设置为接收数据包的起始位置      缓冲器读指针
    ENC28J60_Write(ERDPTL, (NextPacketPtr));
    ENC28J60_Write(ERDPTH, (NextPacketPtr) >> 8);

    // 读取下一个数据包指针
    NextPacketPtr  = ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0);
    NextPacketPtr |= ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0) << 8;

    /************[接收状态向量-->0-15bit]****************/
    // 读取数据包长度 (see datasheet page 46)
    len  = ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0);
    len |= ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0) << 8;
    len -= 4; //删除CRC部分

    /************[接收状态向量-->16-31bit]****************/
    // 读取接收状态 (see datasheet page 45)
    rxstat  = ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0);
    rxstat |= ENC28J60_ReadOp(ENC28J60_READ_BUF_MEM, 0) << 8;
    // 限制检索长度
    if (len > maxlen - 1)
    {
        len = maxlen - 1;
    }

    // 检查CRC是否错误 (see datasheet page 46, table 7-3):
    // The ERXFCON.CRCEN is set by default. 通常我们不需要检查这个
    if ((rxstat & 0x80) == 0) //表示数据包具有有效的 CRC，无符号错误。
    {
        len = 0;
    }
    else //
    {

        ENC28J60_ReadBuffer(len, packet);// 从接收缓冲区复制数据包
    }

    // 将RX读取指针移动到下一个接收数据包的开始处
    // 这会释放我们刚刚读出的内存
    ENC28J60_Write(ERXRDPTL, (NextPacketPtr));
    ENC28J60_Write(ERXRDPTH, (NextPacketPtr) >> 8);

    // 减少数据包计数器表明我们已完成此数据包的处理
    ENC28J60_WriteOp(ENC28J60_BIT_FIELD_SET, ECON2, ECON2_PKTDEC);
    return(len);
}
/*********END*************************以太网层*******************************END*****************/


