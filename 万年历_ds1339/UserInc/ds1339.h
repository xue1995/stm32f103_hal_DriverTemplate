#ifndef _DS1339_HEAD_
#define _DS1339_HEAD_

#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "cmsis_os.h"
#include "hal_iic.h"

/*************�ʼ�****************
1��S1339�����ֲ�.pdf --10ҳ--�Ĵ���˵��
***********************************/
/*��Ҫ�״γ�ʼ��ʱ���������¼Ĵ���*/
#define     REGADDR_SECONDS                         0x00   //��   �Ĵ���
#define     REGADDR_MINUTES                         0x01   //��   �Ĵ���
#define     REGADDR_HOURS                           0x02   //ʱ   �Ĵ���
#define     REGADDR_DAY                             0x03   //���� �Ĵ���
#define     REGADDR_DATA                            0x04   //��   �Ĵ���
#define     REGADDR_MONTH_CENTURY                   0x05   //��   �Ĵ���
#define     REGADDR_YEAR                            0x06   //��   �Ĵ���
/*��Ҫ�������ӣ������������¼Ĵ���������Ĭ�ϼ���*/
#define     REGADDR_ALARM1SECONDS                   0x07   //����1-��   �Ĵ���
#define     REGADDR_ALARM1MINUTES                   0x08   //����1-��   �Ĵ���
#define     REGADDR_ALARM1HOURS                     0x09   //����1-ʱ   �Ĵ���
#define     REGADDR_ALARM1DAY_ALARM1DATE            0x0A   //����1-����-�� �Ĵ���

#define     REGADDR_ALARM2MINUTES                   0x0B   //����2-��   �Ĵ���
#define     REGADDR_ALARM2HOURS                     0x0C   //����2-ʱ   �Ĵ���
#define     REGADDR_ALARM2DAY_ALARM2DATE            0x0D   //����2-����-�� �Ĵ���
/*���¼Ĵ���Ĭ�ϼ���*/
#define     REGADDR_CONTROL                         0x0E   //���ƼĴ������������þ���Ƶ��
#define     REGADDR_STATUS                          0x0F   //״̬
#define     REGADDR_TRICKLECHARGER                  0x10

/*DS1339ʱ����Ϣ�ṹ��*/
typedef struct           //24Сʱ��
{
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t weekday;
    uint8_t day;
    uint8_t month;
    uint8_t year;        //00-99
} T_DS1339TIME;

void DS1339Init(const IIC_PIN *PIN);
IIC_STATUS DS1339SetTime(const IIC_PIN *PIN, const T_DS1339TIME *time);
IIC_STATUS DS1339GetTime(const IIC_PIN *PIN, T_DS1339TIME *time);

#endif
